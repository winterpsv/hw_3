<?php

declare(strict_types=1);

namespace tasks\task3;

/**
 * Class User
 * @package tasks\task3
 */
class User
{
    /** @var string $name */
    public string $name;

    /** @var int $age */
    public int $age;

    /** @param int $newAge */
    public function setAge(int $newAge): void
    {
        ($newAge < 18) ?: $this->age = $newAge;
    }
}
