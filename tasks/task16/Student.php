<?php

declare(strict_types=1);

namespace tasks\task16;

/**
 * Class Student
 * @package tasks\task16
 */
class Student
{
    /** @var string $name */
    public string $name;

    /** @var int $scholarship */
    public int $scholarship;
}
