<?php

declare(strict_types=1);

namespace tasks\task8;

/**
 * Class Student
 * @package tasks\task8
 */
class Student
{
    /** @var string $name */
    public string $name;

    /** @var int $course 1 - 5 */
    public int $course = 1;

    /** @var string $courseAdministrator */
    private string $courseAdministrator;

    /**
     * Set course +1 if course<5
     */
    public function transferToNextCourse(): void
    {
        (!$this->isCourseCorrect()) ?: $this->course = $this->course + 1;
    }

    /**
     * @param string $name
     */
    public function setCourseAdministrator(string $name): void
    {
        $this->courseAdministrator = $name;
    }

    /**
     * @return string
     */
    public function getCourseAdministrator(): string
    {
        return $this->courseAdministrator;
    }

    /**
     * @return bool
     */
    private function isCourseCorrect(): bool
    {
        return ($this->course < 5) ? true : false;
    }
}
