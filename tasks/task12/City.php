<?php

declare(strict_types=1);

namespace tasks\task12;

/**
 * Class City
 * @package tasks\task12
 */
class City
{
    /** @var string $name */
    public string $name;

    /** @var int $foundation */
    public int $foundation;

    /** @var int $population */
    public int $population;
}
