<?php

declare(strict_types=1);

namespace tasks\task11;

/**
 * Class Employee
 * @package tasks\task11
 */
class Employee
{
    /** @var string $name */
    public string $name;

    /** @var int $age */
    public int $age;

    /** @var int $salary */
    public int $salary;

    /**
     * Employee constructor.
     * @param string $name
     * @param int $age
     * @param int $salary
     */
    public function __construct(string $name, int $age, int $salary)
    {
        $this->name = $name;
        $this->age = $age;
        $this->salary = $salary;
    }
}
