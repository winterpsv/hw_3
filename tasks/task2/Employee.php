<?php

declare(strict_types=1);

namespace tasks\task2;

/**
 * Class Employee
 * @package tasks\task2
 */
class Employee
{
    /** @var string $name */
    public string $name;

    /** @var int $age */
    public int $age;

    /** @var int $salary */
    public int $salary;

    /** @return string name */
    public function getName()
    {
        return $this->name;
    }

    /** @return int age */
    public function getAge()
    {
        return $this->age;
    }

    /** @return int salary */
    public function getSalary()
    {
        return $this->salary;
    }

    /** @return bool age=>18 */
    public function checkAge(): bool
    {
        return ($this->age >= 18) ? true : false;
    }
}
