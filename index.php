<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

use tasks\task1\Employee as Task1;
use tasks\task2\Employee as Task2;
use tasks\task3\User as Task3;
use tasks\task6\User as Task6;
use tasks\task8\Student as Task8;
use tasks\task11\Employee as Task11;
use tasks\task12\City as Task12;
use tasks\task14\User as Task14;
use tasks\task16\Employee as Task16Employee;
use tasks\task16\Student as Task16Student;

/*
 * Task 1
 */
$task1_1 = new Task1();
$task1_1->name = 'Иван';
$task1_1->age = 25;
$task1_1->salary = 1000;

$task1_2 = new Task1();
$task1_2->name = 'Вася';
$task1_2->age = 26;
$task1_2->salary = 2000;

/*
 * Result Task 1
 */
echo $task1_1->salary + $task1_2->salary . "<br>";
echo $task1_1->age + $task1_2->age . "<br>";

/*
 * Task 2
 */
$task2_1 = new Task2();
$task2_1->name = 'Коля';
$task2_1->age = 35;
$task2_1->salary = 2400;

$task2_2 = new Task2();
$task2_2->name = 'Толя';
$task2_2->age = 41;
$task2_2->salary = 4300;

/*
 * Result Task 2
 */
echo $task2_1->getSalary() + $task2_2->getSalary() . "<br>";

/*
 * Task 3
 */
$task3_1 = new Task3();
$task3_1->name = 'Коля';
$task3_1->age = 25;
$task3_1->setAge(30);

/*
 * Result Task 3
 */
echo $task3_1->age . "<br>";

/*
 * Task 6
 */
$task6_1 = new Task6();
$task6_1->setAge(18);
$task6_1->addAge(5);
$task6_1->subAge(8);
$task6_1->setAge(16);
$task6_1->subAge(3);

/*
 * Result Task 6
 */
echo $task6_1->age . "<br>";

/*
 * Task 7
 */
//print_r($task6_1->validateAge(13));

/*
 * Task 8
 */
$task8_1 = new Task8();
$task8_1->transferToNextCourse();
$task8_1->transferToNextCourse();
$task8_1->transferToNextCourse();
$task8_1->transferToNextCourse();
$task8_1->transferToNextCourse();
$task8_1->transferToNextCourse();
$task8_1->transferToNextCourse();
//$task8_1->courseAdministrator = 'Admin';

/*
 * Result Task 8
 */
echo $task8_1->course . "<br>";

/*
 * Task 11
 */
$task11_1 = new Task11('Вася', 25, 1000);
$task11_2 = new Task11('Петя', 30, 2000);

/*
 * Result Task 11
 */
echo $task11_1->salary + $task11_2->salary . "<br>";

/*
 * Task 12
 */
$task12_1 = new Task12();
$task12_1->name = 'Обухов';
$task12_1->foundation = 1362;
$task12_1->population = 2884;
$props = get_object_vars($task12_1);

/*
 * Result Task 12
 */
foreach ($props as $key => $value) {
    echo $key . ' - ' . $value . "<br>";
}

/*
 * Task 13
 */
$task13_1 = new Task2();
$task13_1->name = 'Андрей';
$task13_1->age = 33;
$task13_1->salary = 6500;

$methods = ['method1' => 'getName', 'method2' => 'getAge'];
$method1 = $methods['method1'];
$method2 = $methods['method2'];

/*
 * Result Task 13
 */
echo $task13_1->getAge() . "<br>";
echo $task13_1->$method1() . "<br>";
echo $task13_1->$method2() . "<br>";

/*
 * Task 14
 */
$task14_1 = new Task14();

/*
 * Result Task 14
 */
echo $task14_1->setSurname('Pedchenko')->setName('Sergey')->setPatronymic('Vladimirovich')->getFullName() . "<br>";
echo $task14_1->setPatronymic('Alexandrovich')->setName('Vasya')->setSurname('Pypkin')->getFullName() . "<br>";

/*
 * Task 16
 */
$arr = [];
$task16_e_1 = new Task16Employee();
$task16_e_1->name = 'Вася';
$task16_e_1->salary = 4300;
$arr[] = $task16_e_1;

$task16_e_2 = new Task16Employee();
$task16_e_2->name = 'Петя';
$task16_e_2->salary = 6500;
$arr[] = $task16_e_2;

$task16_e_3 = new Task16Employee();
$task16_e_3->name = 'Рома';
$task16_e_3->salary = 2400;
$arr[] = $task16_e_3;

$task16_s_1 = new Task16Student();
$task16_s_1->name = 'Жора';
$task16_s_1->salary = 1100;
$arr[] = $task16_s_1;

$task16_s_2 = new Task16Student();
$task16_s_2->name = 'Коля';
$task16_s_2->salary = 2300;
$arr[] = $task16_s_2;

$task16_s_3 = new Task16Student();
$task16_s_3->name = 'Толя';
$task16_s_3->salary = 1500;
$arr[] = $task16_s_3;

/*
 * Result Task 16
 */
//all Employee
foreach ($arr as $value) {
    $class_name = get_class($value);
    if(strpos($class_name, 'Employee')) {
        echo $value->name . '<br>';
    }
}

//all Student
foreach ($arr as $value) {
    $class_name = get_class($value);
    if(strpos($class_name, 'Student')) {
        echo $value->name . '<br>';
    }
}

//sum Employee and Student
$sumEmployee = 0;
$sumStudent = 0;
foreach ($arr as $value) {
    $class_name = get_class($value);
    if(strpos($class_name, 'Employee')) {
        $sumEmployee = $sumEmployee + $value->salary;
    } elseif (strpos($class_name, 'Student')) {
        $sumStudent = $sumStudent + $value->salary;
    }
}

//sum result
echo $sumEmployee . '<br>';
echo $sumStudent . '<br>';
